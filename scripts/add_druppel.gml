///add_druppel(x, y)
var xd = argument0;
var yd = argument1;

with(regen)
{
    xx[druppels] = xd;
    yy[druppels] = yd;
    zz[druppels] = random_range(500, 600);
    gz[druppels] = get_height(xd, yd);
    hspd[druppels] = random_range(-1.5, 0);
    vspd[druppels] = random_range(-1, 1);
    zspd[druppels] = random_range(-12, -10);
    rr[druppels] = random_range(1.5, 2.5);
    druppels++;
}

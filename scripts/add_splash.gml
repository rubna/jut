///add_splash(x, y, r)
var xx = argument0;
var yy = argument1;
var rr = argument2;

with(world)
{
    splash_x[splashes] = xx;
    splash_y[splashes] = yy;
    splash_r[splashes] = rr;
    splashes++;
}

///bezier_3d_water_shadow(x1, y1, z1, x2, y2, z2, x3, y3, z3, width, steps)
var x1 = argument0;
var y1 = argument1;
var z1 = argument2;
var x2 = argument3;
var y2 = argument4;
var z2 = argument5;
var x3 = argument6;
var y3 = argument7;
var z3 = argument8;
var w = argument9;
var step_size = 1 / argument10;

var xnet = -1;
var ynet = -1;
var znet = -1;
var gznet = -1;
for (var i = 0; i <= 1; i+= step_size)
{
    var x12 = lerp(x1, x2, i);
    var y12 = lerp(y1, y2, i);
    var z12 = lerp(z1, z2, i);
    var x23 = lerp(x2, x3, i);
    var y23 = lerp(y2, y3, i);
    var z23 = lerp(z2, z3, i);
    
    var xx = lerp(x12, x23, i);
    var yy = lerp(y12, y23, i);
    var zz = lerp(z12, z23, i);
    
    var z_echt = zz;
    if (i > 0 && zz <= world.water_height)
    {
        var water_t = (world.water_height - znet) / (zz - znet)
        xx = lerp(xnet, xx, water_t);
        yy = lerp(ynet, yy, water_t);
        zz = lerp(znet, zz, water_t);
    }
    
    var gz = get_height(xx, yy);
    if (i > 0 && i < 1)
        circle_3d_shadow_known(xx, yy, zz, gz, w / 2);
    if (i > 0)
        line_3d_shadow_known(xnet, ynet, znet, xx, yy, zz, gznet, gz, w);
    xnet = xx;
    ynet = yy;
    znet = zz;
    gznet = gz;
    
    if (z_echt <= world.water_height)
        break;
}

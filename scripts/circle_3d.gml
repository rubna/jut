///circle_3d(x, y, z, r, xscale{1},yscale{1},angle{0});
var xp,yp,zp,r,xscale,yscale,angle;
xp=argument[0];
yp=argument[1];
zp=argument[2];
r=argument[3];
xscale=1;
yscale=1;
if (argument_count > 4)
{
    xscale=argument[4];
    yscale=argument[5];
}

angle = 0;
if (argument_count > 6)
    angle=argument[6];
    
var rmax = max(r * xscale, r * yscale) * camera.zoom;
var spr = spr_circle_128;
var scale = r / 128;
if (rmax <= 8)
{
    spr = spr_circle_8;
    scale = r / 8;
}
else
if (rmax <= 16)
{
    spr = spr_circle_16;
    scale = r / 16;
}
else
if (rmax <= 32)
{
    spr = spr_circle_32;
    scale = r / 32;
}
else
if (rmax <= 64)
{
    spr = spr_circle_64;
    scale = r / 64;
}
else
if (rmax <= 128)
{
    spr = spr_circle_128;
    scale = r / 128;
}

// draai naar cam angle
var dst = point_distance(camera.rot_x, camera.rot_y, xp, yp);
var dr = point_direction(camera.rot_x, camera.rot_y, xp, yp) + camera.angle;
xp = camera.rot_x + lengthdir_x(dst, dr);
yp = camera.rot_y + lengthdir_y(dst, dr) * camera.yscale;

draw_sprite_ext(spr, 0, (xp- camera.x) * camera.zoom , (yp - camera.y - zp) * camera.zoom, xscale * scale * camera.zoom, yscale * scale * camera.zoom, angle, draw_get_colour(), draw_get_alpha());

///circle_3d_shadow(x, y, z, ground_z, r, xscale{1},yscale{1},angle{0});
var xp,yp,zp,r,xscale,yscale,angle;
xp=argument[0];
yp=argument[1];
zp=argument[2];
r=argument[3];
xscale=1;
yscale=1;
if (argument_count > 4)
{
    xscale=argument[4];
    yscale=argument[5];
}

angle = 0;
if (argument_count > 6)
    angle=argument[6];
    
var rmax = max(r * xscale, r * yscale) * camera.zoom;
var spr = spr_circle_128;//256;
var scale = r / 128;
if (rmax <= 8)
{
    spr = spr_circle_8;
    scale = r / 8;
}
else
if (rmax <= 16)
{
    spr = spr_circle_16;
    scale = r / 16;
}
else
if (rmax <= 32)
{
    spr = spr_circle_32;
    scale = r / 32;
}
else
if (rmax <= 64)
{
    spr = spr_circle_64;
    scale = r / 64;
}
else
if (rmax <= 128)
{
    spr = spr_circle_128;
    scale = r / 128;
}

// shadow offset
var gz = max(world.water_height, get_height(xp, yp, false));
xp += lengthdir_x(max(0, (zp - gz)) * camera.shadow_scale, camera.shadow_dir);
yp += lengthdir_y(max(0, (zp - gz)) * camera.shadow_scale, camera.shadow_dir) * camera.yscale;

draw_sprite_ext(spr, 0, (xp- camera.x) * camera.zoom , (yp - camera.y - gz) * camera.zoom, xscale * scale * camera.zoom, yscale * scale * camera.zoom, angle, draw_get_colour(), draw_get_alpha());

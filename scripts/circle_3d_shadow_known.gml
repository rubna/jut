///circle_3d_shadow_known(x, y, z, ground_z, r, xscale{1},yscale{1},angle{0});
var xp,yp,zp,r,xscale,yscale,angle;
xp=argument[0];
yp=argument[1];
zp=argument[2];
gz=argument[3];
r=argument[4];
xscale=1;
yscale=1;
if (argument_count > 5)
{
    xscale=argument[5];
    yscale=argument[6];
}

angle = 0;
if (argument_count > 7)
    angle=argument[7];
    
var rmax = max(r * xscale, r * yscale) * camera.zoom;
var spr = spr_circle_128;//256;
var scale = r / 128;
if (rmax <= 8)
{
    spr = spr_circle_8;
    scale = r / 8;
}
else
if (rmax <= 16)
{
    spr = spr_circle_16;
    scale = r / 16;
}
else
if (rmax <= 32)
{
    spr = spr_circle_32;
    scale = r / 32;
}
else
if (rmax <= 64)
{
    spr = spr_circle_64;
    scale = r / 64;
}
else
if (rmax <= 128)
{
    spr = spr_circle_128;
    scale = r / 128;
}

// shadow offset
gz = max(world.water_height, gz);
xp += lengthdir_x((zp - gz) * camera.shadow_scale, camera.shadow_dir);
yp += lengthdir_y((zp - gz) * camera.shadow_scale, camera.shadow_dir) * camera.yscale;

draw_sprite_ext(spr, 0, (xp- camera.x) * camera.zoom , (yp - camera.y - gz) * camera.zoom, xscale * scale * camera.zoom, yscale * scale * camera.zoom, angle, draw_get_colour(), draw_get_alpha());

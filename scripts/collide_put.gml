///collide_put();
if (!instance_exists(put))
    exit;
var dst = point_distance(x, y * 1, put.x + put.xoff, put.y * 1);
if (dst < put.radius - put.dikte / 2)
    in_put = true;
if (dst > put.radius - put.dikte - radius && dst < put.radius + radius)
{
    put.zoff *= 1.5;
    if (z < put.z + put.zoff)
    {
        var dr = point_direction(put.x + put.xoff, put.y, x, y);
        if (dst < put.radius - put.dikte / 2)
        {
            x = put.x + put.xoff + lengthdir_x(put.radius - put.dikte - radius, dr);
            y = put.y + lengthdir_y(put.radius - put.dikte - radius, dr) * 1;
        }
        else
        {
            x = put.x + put.xoff + lengthdir_x(put.radius + radius, dr);
            y = put.y + lengthdir_y(put.radius + radius, dr) * 1;
        }
    }
    else
    if (z < put.z + put.zoff + radius / 2)
    {
        var dr = point_direction(put.x + put.xoff, put.y, x, y);
        z = put.z + put.zoff + radius / 2;
        if (zspd < 0)
            zspd *= -bounciness;
        var s = dst - (put.radius - put.dikte / 2);
        s /= 10;
        s = clamp(s, -0.3, 0.3);
        if (abs(s) < 0.01)
            s = 0.01 * getsign(s);
        hspd += lengthdir_x(s, dr);
        vspd += lengthdir_y(s, dr);
    }
    put.zoff /= 1.5;
}

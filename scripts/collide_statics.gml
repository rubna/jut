///collide_statics(bounce, xdrag, ydrag);
var collided = noone;

with(static)
{
    var rr = other.radius + radius;
    if (collision)
    { 
        var d = distance_to_line(left_x, left_y, right_x, right_y, other.x, other.y);
        var line_coll = false;
        if ((collision_flip == 0 && abs(d) < rr) || (collision_flip!=0 && d * collision_flip < rr))
        {
            if (point_distance(other.x, other.y, right_x, right_y) < length)
            {
                var left_dist = point_distance(left_x, left_y, other.x, other.y);
                var perp_dist = d;
                var dist_on_line = sqrt(sqr(left_dist) - sqr(perp_dist));
                if (dist_on_line < length)
                {
                    line_coll = true;
                    var line_dr = point_direction(left_x, left_y, right_x, right_y);
                    var flippp = collision_flip;
                    if (flippp == 0)
                        flippp = getsign(d);
                    var xnew = left_x + lengthdir_x(dist_on_line, line_dr) + lengthdir_x(rr, line_dr + 90 * flippp);
                    var ynew = left_y + lengthdir_y(dist_on_line, line_dr) + lengthdir_y(rr, line_dr + 90 * flippp);
                    if (abs(xnew - other.x) > 0.1)
                        other.x = xnew;
                    if (abs(ynew - other.y) > 0.1)
                        other.y = ynew;
                    
                    collided =  self.id;
                }
            }
            
            // rechter punt
            if (!line_coll && point_distance(right_x, right_y, other.x, other.y) < rr)
            {
                var dr = point_direction(right_x, right_y, other.x, other.y);
                other.x = right_x + lengthdir_x(rr, dr);
                other.y = right_y + lengthdir_y(rr, dr);
                collided = self.id;
                //with(other)
                //    bounce_direction(dr - 90, argument0, argument1, argument2, 1);
                //return self.id; 
            }
            
             // linker punt
            if (!line_coll && collide_left && point_distance(left_x, left_y, other.x, other.y) < rr)
            {
                var dr = point_direction(left_x, left_y, other.x, other.y);
                other.x = left_x + lengthdir_x(rr, dr);
                other.y = left_y + lengthdir_y(rr, dr);
                
                //with(other)
                //    bounce_direction(dr - 90, argument0, argument1, argument2, 1);
                
                collided = self.id;
            }
        }
    }
}// stuiter op pad
return collided;

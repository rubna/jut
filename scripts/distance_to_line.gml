///distance_to_line(x1, y1, x2, y2, xp, yp)
var x1 = argument0;
var y1 = argument1;
var x2 = argument2;
var y2 = argument3;
var xp = argument4;
var yp = argument5;

var a = (y2 - y1) * xp - (x2 - x1) * yp + x2 * y1 - y2 * x1;
a /= point_distance(x1, y1, x2, y2);
return a;

for (var j = 0; j < 3; j ++)
   {
       var f = j / 3;
       if (i == 0)
           f = 1 - f;
       var a = image_angle + lerp(-30, 30, f); //  point_direction(x, y, x, y)
       
       f = j / 3;
       var teen_len = radius * lerp(1, 0.66, f) * 0.8;
       var teen_w   = 4 * lerp(1, 1.4, f); 
       
       var teen_x = x + lengthdir_x(teen_len, a);
       var teen_y = y + lengthdir_y(teen_len, a);
       var teen_z = z;
       line_3d(x, y, z,
               teen_x, teen_y, teen_z, teen_w);
       teen_w /= 2;
       circle_3d(teen_x, teen_y, teen_z, teen_w);
       //circle_3d(x, y, z, teen_w);
       
  } 

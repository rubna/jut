///ease(x, begin, end)
var t = clamp(argument0, 0, 1);
var start = argument1;
var change = argument2 - start;
t *= 2;
if (t < 1) 
    return change/2*t*t + start;
t--;
return -change/2 * (t*(t-2) - 1) + start;

///get_height(x, y, put{=true});
if (!world.loaded)
    return -1;

var xx = argument[0];
var yy = argument[1];
xx -= world.x;
yy -= world.y;

xx /= world.image_xscale;
yy /= world.image_yscale;

xx -= 0.5;
yy -= 0.5;
var x_int = floor(xx);
var y_int = floor(yy);
var x_res = xx mod 1;
var y_res = yy mod 1;

// out of bounds check
if (x_int < 0 || y_int < 0 || x_int >= world.map_width - 1 || y_int >= world.map_height - 1)
    return -1;

// interpolate height
var h_00 = world.heights[x_int, y_int];
var h_10 = world.heights[x_int + 1, y_int];
var h_01 = world.heights[x_int, y_int + 1];
var h_11 = world.heights[x_int + 1, y_int + 1];
var sum = 0;
sum += (x_res * y_res) * h_11;
sum += ((1 - x_res) * y_res) * h_01;
sum += (x_res * (1 - y_res)) * h_10;
sum += ((1 - x_res) * (1 - y_res)) * h_00;

// wobble
sum += (sin((xx / 100) * 130) + cos((yy / 100) * 170.)) * 0.02 * 255;
    
// normalize to worlds max_height
sum *= world.max_height;
sum /= 255;

if ((argument_count <= 2 || argument[2]) && instance_exists(put))
    if (point_distance(x, y, put.x, put.y) < put.radius - put.dikte / 2)
        sum -= 6;
return sum;

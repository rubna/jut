///initialize_game()
instance_create(0, 0, aanspoeler);
if (file_exists(savefile))
    load_state();
else
{
    // initial game state
    instance_create(1350, 800, man);
    var h = instance_create(man.x, man.y, hoed);
    with(man)
        stop_in_buik(h);
    
    aanspoeler.last_date = date_inc_hour(date_current_datetime(), -1);
    
    /*var iiii = 0;
    with(aanspoeler)
    {
        while(seconds_since_saved > aanspoel_tijd())
        {
           seconds_since_saved -= aanspoel_tijd();
            if (aantal_items < max_items)
            {
               iiii++;
               var t = random(1);
               var xooo = -800 + random_range(-120, 120);
               var xx = lerp(x1 + xooo, x2 + xooo, t);
               var yy = lerp(y1, y2, t);
               var zz = get_height(xx, yy);
               
               spawn_item(xx, yy, zz);
            }
        }
        alarm[0] = (aanspoel_tijd() - seconds_since_saved) * 60; // 60 faps
    }
    show_debug_message("Spawned " + string(iiii) + " items while u were gone!");*/
}

instance_create(man.x - view_wview / (2 * pixel_scale * 1.3), man.y - view_hview / (2 * pixel_scale * 1.3), camera);
instance_create(0, 0, mouse);
if (world.all_found == false)
    instance_create(0, 0, regen);

// intro!
my_intro = instance_create(0, 0, intro);
if (fullscreen)
    toggle_fullscreen();

   

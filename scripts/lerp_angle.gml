///lerp_angle(angle, target, t)
var angle = argument0;
var target = argument1;
var t = argument2;
return -angle_difference(angle, target) * t;

///line_3d(x1, y1, z1, x2, y2, z2, width_1, c1{=def})
var x1 = argument[0];
var y1 = argument[1];
var z1 = argument[2];
var x2 = argument[3];
var y2 = argument[4];
var z2 = argument[5];
var w1 = argument[6] / 2;
var c1 = draw_get_color();
if (argument_count > 7)
    c1 = argument[7];
    
    
// camera angle offset
/*var dst = point_distance(camera.rot_x, camera.rot_y, x1, y1);
var dr = point_direction(camera.rot_x, camera.rot_y, x1, y1) + camera.angle;
x1 = camera.rot_x + lengthdir_x(dst, dr);
y1 = camera.rot_y + lengthdir_y(dst, dr) * camera.yscale;
y1 -= z1;

var dst = point_distance(camera.rot_x, camera.rot_y, x2, y2);
var dr = point_direction(camera.rot_x, camera.rot_y, x2, y2) + camera.angle;
x2 = camera.rot_x + lengthdir_x(dst, dr);
y2 = camera.rot_y + lengthdir_y(dst, dr) * camera.yscale;*/

y1 -= z1;
y2 -= z2;

var dr = point_direction(x1, y1, x2, y2);
var dst = point_distance(x1, y1, x2, y2);

draw_sprite_ext(spr_pixel, 0, ((x1 + x2) / 2 - camera.x) * camera.zoom, ((y1 + y2) / 2 - camera.y) * camera.zoom, dst * camera.zoom / 2, w1 * camera.zoom, dr, c1, draw_get_alpha());


///line_3d_shadow_known(x1, y1, z1, x2, y2, z2, ground_z1, ground_z2, width_1, c1{=def})
var x1 = argument[0];
var y1 = argument[1];
var z1 = argument[2];
var x2 = argument[3];
var y2 = argument[4];
var z2 = argument[5];
var gz1 = argument[6];
var gz2 = argument[7];
var w1 = argument[8] / 2;
var c1 = draw_get_color();
if (argument_count > 9)
    c1 = argument[9];

// shadow offset 1
gz1 = max(world.water_height, gz1);
x1 += lengthdir_x((z1 - gz1) * camera.shadow_scale, camera.shadow_dir);
y1 += lengthdir_y((z1 - gz1) * camera.shadow_scale, camera.shadow_dir) * camera.yscale;
y1 -= gz1;
gz2 = max(world.water_height, gz2);
x2 += lengthdir_x((z2 - gz2) * camera.shadow_scale, camera.shadow_dir);
y2 += lengthdir_y((z2 - gz2) * camera.shadow_scale, camera.shadow_dir) * camera.yscale;
y2 -= gz2;


var dr = point_direction(x1, y1, x2, y2);
var dst = point_distance(x1, y1, x2, y2);

draw_sprite_ext(spr_pixel, 0, ((x1 + x2) / 2 - camera.x) * camera.zoom, ((y1 + y2) / 2 - camera.y) * camera.zoom, dst * camera.zoom / 2, w1 * camera.zoom, dr, c1, 1);


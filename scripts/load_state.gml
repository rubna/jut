///load_state();
ini_open(savefile);

// load gevonden items
with(world)
{
    for (var i = 0; i < finds_amount; i++)
    {
        var str = ini_read_string("finds", string(i), "no");
        if (str == finds_names[i])
            found[i] = true;
    }
    
    // alles gevonden?
    all_found = true;
    for (var i = 0; i < finds_amount - 1; i++)
        if (!found[i])
            all_found = false;
    
}

// load man
var man_x = ini_read_real("man_section", "x_pos", 1000);
var man_y = ini_read_real("man_section", "y_pos", 1000);
var man_z = ini_read_real("man_section", "z_pos", 1000);
instance_create(man_x, man_y, man);
man.z = get_height(man_x, man_y);

// load items
var item_count = ini_read_real("game_state", "item_count", 0);

for (var i = 0; i < item_count; i++)
{
    var section = "item_"+string(i);
    var obj = asset_get_index(ini_read_string(section,"object_index", -1));
    var xx = ini_read_real(section, "x_pos", 1000);
    var yy = ini_read_real(section, "y_pos", 1000);
    var zz = ini_read_real(section, "z_pos", 100);
    var in_buik = ini_read_real(section, "in_buik", 0);
    var on_head = ini_read_real(section, "on_head", 0);
    var hat_index = ini_read_real(section, "hat_index", -1);
    // add instance
    if (obj != -1)
    {
        var inst;
        if (in_buik)
        {
            inst = instance_create(man.x + xx, man.y, obj);
            inst.z = man.z + yy; // yy == buik_zoff
        }
        else
        {
            inst = instance_create(xx, yy, obj);
            inst.z = zz;//get_height(xx, yy);
        }
        
        inst.in_buik = in_buik
        if (in_buik)
        {
            man.buik_dingen++;
        }
        inst.on_head = on_head
        inst.hat_index = hat_index;
        if (on_head)
        {
            man.my_hat[hat_index] = inst;
            man.hats = max(man.hats, hat_index + 1);
        }
    }
}


aanspoeler.loaded = true;
aanspoeler.aantal_items = item_count;
aanspoeler.last_date = ini_read_real("game_state", "last_date", date_current_datetime());
aanspoeler.time_span = ini_read_real("game_state", "time_span", date_current_datetime());

// load settings
pixel_scale = ini_read_real("game_state", "pixel_scale", 2);
fullscreen = ini_read_real("game_state", "fullscreen", true);

ini_close();

// spawn items aangespoeld terwijl je weg was
/*aanspoeler.seconds_since_saved = date_second_span( aanspoeler.saved_date, date_current_datetime());
show_debug_message(string(aanspoeler.seconds_since_saved) + " seconds elapsed from last play-session.");
var iiii = 0;
with(aanspoeler)
{
    seconds_since_saved *= 60;
    while(seconds_since_saved > aanspoel_tijd())
    {
       seconds_since_saved -= aanspoel_tijd();
        if (aantal_items < max_items)
        {
           iiii++;
           var t = random(1);
           var xooo = -800 + random_range(-120, 120);
           var xx = lerp(x1 + xooo, x2 + xooo, t);
           var yy = lerp(y1, y2, t);
           var zz = get_height(xx, yy);
           
           spawn_item(xx, yy, zz);
        }
    }
    alarm[0] = (aanspoel_tijd() - seconds_since_saved) * 60; // 60 faps
}
show_debug_message("Spawned " + string(iiii) + " items while u were gone!");*/



///look_up_name(name)
with(world)
{
    for (var i = 0; i < finds_amount; i++)
        if (argument0 == finds_names[i])
            return i;
    return -1;
}

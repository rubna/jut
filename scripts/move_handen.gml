for (var i = 0; i < 2; i++)
{
    if (hand_obj[i].grabbing && hand_t[i] < 1 && hand_obj[i].my_item == noone)
    {
        hand_t_spd[i] = min(0.1, 3 / point_distance_3d(hand_xlast[i], hand_ylast[i], hand_zlast[i], hand_xtarg[i], hand_ytarg[i], hand_ztarg[i]));
        if (hand_t[i] < 1)
            hand_t[i] += hand_t_spd[i];
        if (hand_t[i] >= 1)
            hand_t[i] = 1;
        
        hand_obj[i].x = ease(hand_t[i], hand_xlast[i], hand_xtarg[i]);
        hand_obj[i].y = ease(hand_t[i], hand_ylast[i], hand_ytarg[i]);
        hand_obj[i].z = max(hand_obj[i].ground_z, ease(hand_t[i], hand_zlast[i], hand_ztarg[i]));
    }
    else
    {
        var xplus = (hand_xtarg[i] - hand_obj[i].x) * hand_obj[i].lerp_spd * (1 - crouch);;
        var yplus = (hand_ytarg[i] - hand_obj[i].y) * hand_obj[i].lerp_spd * (1 - crouch);;
        var zplus = (hand_ztarg[i] - hand_obj[i].z) * hand_obj[i].lerp_spd * (1 - crouch);;
        var plus = point_distance_3d(0, 0, 0, xplus, yplus, zplus);
        plus = min(plus, 10);
        var xydir = point_direction(0, 0, xplus, yplus);
        var xydist = point_distance(0, 0, xplus, yplus);
        var zdir = point_direction(0, 0, xydist, zplus);
        
        xplus = lengthdir_x(lengthdir_x(plus, zdir), xydir);
        yplus = lengthdir_y(lengthdir_x(plus, zdir), xydir);
        zplus = lengthdir_y(plus, zdir);
        
        hand_obj[i].x += xplus;
        hand_obj[i].y += yplus;
        hand_obj[i].z += zplus;
        hand_obj[i].z  = max(hand_obj[i].ground_z, hand_obj[i].z);
    }
}

for (var i = 0; i < 2; i++)
{
    if (voet_t[i] < 1)
    {
        voet_t[i] += 0.013;
        //var f = 1 + lengthdir_y(0.66, voet_t[i] * 180);
        //loop_spd *= f;
        body.wobble = max(body.wobble, voet_t[i]);
        
        if (voet_t[i] >= 1)
        {
            var v = instance_create(voet_xtarg[i], voet_ytarg[i], voetstap);
            v.z = voet_ztarg[i];
            v.image_yscale *= i * 2 - 1;
            v.image_angle = voet_atarg[i] + (i * 2 - 1) * 10;
            v.i = i;
        }
    }
    if (voet_t[i] >= 1)
    {
        voet_t[i] = 1;
    }
    
    var v_xprev = voet_x[i];
    var v_yprev = voet_y[i];
    voet_x[i] = ease(voet_t[i], voet_xlast[i], voet_xtarg[i]);
    voet_y[i] = ease(voet_t[i], voet_ylast[i], voet_ytarg[i]);
    voet_z[i] = ease(voet_t[i], voet_zlast[i], voet_ztarg[i]);
    voet_a[i] = voet_alast[i] + lerp_angle(voet_alast[i], voet_atarg[i], voet_t[i]);
    
    voet_z[i] -= lengthdir_y(radius, voet_t[i] * 180);
    
    if (world.loaded)
    {
        var dst = point_distance(voet_x[i], voet_y[i], v_xprev, v_yprev);
        world.rustle_volume = max(world.rustle_volume, clamp(dst / 2, 0, 1));
        
        if (voet_in_water[i])
        {
            var dst = point_distance(voet_x[i], voet_y[i], v_xprev, v_yprev);
            world.waad_volume = max(world.waad_volume, clamp(dst / 2, 0, 1));
        }
    }
    
    // water splashes
    if (voet_z[i] < world.water_height && !voet_in_water[i])
    {
        voet_in_water[i] = true;
        if (voet_zlast[i] > world.water_height)
            sound(sou_water_plons, voet_x[i], voet_y[i] - voet_z[i], 1, random_range(0.9, 1));
        else
            sound(choose(sou_water_stap_0, sou_water_stap_1, sou_water_stap_2), voet_x[i], voet_y[i] - voet_z[i], 1, random_range(0.75, 0.85));
        add_splash(voet_x[i], voet_y[i], 10);
    }
    if (voet_z[i] > world.water_height && voet_in_water[i])
    {
        voet_in_water[i] = false;
        //sound(sou_water_stap, voet_x[i], voet_y[i] - voet_z[i], 1, 0.7);
        add_splash(voet_x[i], voet_y[i], 6);
    }
    
}

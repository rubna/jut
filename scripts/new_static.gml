///new_static(x1, y1, x2, y2, radius, collide_left{=true}, collision_flip{=0});

var s = instance_create(0, 0, static);
s.left_x = argument[0];
s.left_y = argument[1];
s.right_x = argument[2];
s.right_y = argument[3];
s.radius = argument[4];
s.length = point_distance(s.left_x, s.left_y, s.right_x, s.right_y);
if (argument_count > 5 && !argument[5])
    s.collide_left = false;
if (argument_count > 6)
    s.collision_flip = argument[6];
return s;

///quadratic_bezier_3d_water(x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, width, steps);
var x1 = argument0;
var y1 = argument1;
var z1 = argument2;

var x2 = argument3;
var y2 = argument4;
var z2 = argument5;

var x3 = argument6;
var y3 = argument7;
var z3 = argument8;

var x4 = argument9;
var y4 = argument10;
var z4 = argument11;
var w = argument12;
var step_size = 1 / argument13;

var xnet = -1;
var ynet = -1;
var znet = -1;
var in_water = false;
for (var i = 0; i <= 1; i+= step_size)
{
    var x12 = lerp(x1, x2, i);
    var y12 = lerp(y1, y2, i);
    var z12 = lerp(z1, z2, i);
    var x23 = lerp(x2, x3, i);
    var y23 = lerp(y2, y3, i);
    var z23 = lerp(z2, z3, i);
    var x34 = lerp(x3, x4, i);
    var y34 = lerp(y3, y4, i);
    var z34 = lerp(z3, z4, i);
    
    var xx1 = lerp(x12, x23, i);
    var yy1 = lerp(y12, y23, i);
    var zz1 = lerp(z12, z23, i);
    var xx2 = lerp(x23, x34, i);
    var yy2 = lerp(y23, y34, i);
    var zz2 = lerp(z23, z34, i);
    
    var xx = lerp(xx1, xx2, i);
    var yy = lerp(yy1, yy2, i);
    var zz = lerp(zz1, zz2, i);
    
    var z_echt = zz;
    if (i > 0 && zz <= world.water_height)
    {
        var water_t = (world.water_height - znet) / (zz - znet)
        xx = lerp(xnet, xx, water_t);
        yy = lerp(ynet, yy, water_t);
        zz = lerp(znet, zz, water_t);
        
        add_splash(xx, yy, w);
    }
    circle_3d(xx, yy, zz, w / 2);
    if (i > 0)
        line_3d(xnet, ynet, znet, xx, yy, zz, w);
    xnet = xx;
    ynet = yy;
    znet = zz;
    
    if (z_echt <= world.water_height)
        break;
}

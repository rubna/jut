///resize_application_surface();
view_wview = window_get_width() / world.pixel_scale;
view_hview = window_get_height() / world.pixel_scale;
view_wport = view_wview;
view_hport = view_hview;
surface_resize(application_surface, view_wview, view_hview);

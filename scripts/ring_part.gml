///ring_part(xp,yp,r, width, vertices, xscale{1},yscale{1},angle{0}, part{=1});
var xp,yp,r, width, vertices,xscale,yscale,angle, part;
xp=argument[0];
yp=argument[1] * 0.75;
r=argument[2];
width = argument[3]
vertices = argument[4];
xscale=1;
yscale=1;
if (argument_count > 5)
{
    xscale=argument[5];
    yscale=argument[6];
}

angle = 0;
if (argument_count > 7)
    angle=argument[7];

part = 1
if (argument_count > 8)
    part = argument[8];

draw_primitive_begin(pr_trianglestrip);
vertex(xp,yp);
var i;
for (i=0;i<vertices+1;i+=1)
{
    if ((i) / vertices > part)
        break;
    var dr,dst;
    dr=point_direction(0,0,xscale*lengthdir_x(r,360/vertices*i),yscale*lengthdir_y(r,360/vertices*i))+angle;
    dst=point_distance (0,0,xscale*lengthdir_x(r,360/vertices*i),yscale*lengthdir_y(r,360/vertices*i));
    vertex(xp+lengthdir_x(dst,dr),yp + lengthdir_y(dst,dr));
    
    dr=point_direction(0,0,xscale*lengthdir_x(r - width / xscale,360/vertices*i),yscale*lengthdir_y(r - width / yscale,360/vertices*i))+angle;
    dst=point_distance (0,0,xscale*lengthdir_x(r - width / xscale,360/vertices*i),yscale*lengthdir_y(r - width / yscale,360/vertices*i));
    vertex(xp+lengthdir_x(dst,dr),yp + lengthdir_y(dst,dr));
}
draw_primitive_end();

///save_item(obj, index);
var section;
section = "item_"+string(argument1);
var s = "";
if (argument0 == noone)
    show_debug_message("BUG: Saved no-one!");
else
with(argument0)
{
    ini_write_string(section, "object_index", object_get_name(object_index));
    
    // check of ik of ander in buik zit
    var zit_in_buik = in_buik;
    if (ander != noone)
        zit_in_buik = max(in_buik, ander.in_buik);
    
    // write buik stuff!
    ini_write_real(section, "in_buik", zit_in_buik);
    ini_write_real(section, "z_pos", floor(z));
    if (!zit_in_buik)
    {
        ini_write_real(section, "x_pos", floor(x));
        ini_write_real(section, "y_pos", floor(y));
    }
    else
    {
        if (in_buik)
        {
            ini_write_real(section, "x_pos", buik_xoff);
            ini_write_real(section, "y_pos", buik_zoff);
        }
        else
        {
            ini_write_real(section, "x_pos", ander.buik_xoff);
            ini_write_real(section, "y_pos", ander.buik_zoff);
        }
    }
    
    // write op hoofd
    ini_write_real(section, "on_head", on_head);
    ini_write_real(section, "hat_index", hat_index);
}

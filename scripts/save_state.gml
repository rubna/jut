///save_state();
ini_open(savefile);

// save gevonden items
with(world)
{
    for (var i = 0; i < finds_amount; i++)
        if (found[i])
            ini_write_string("finds", string(i), finds_names[i]);
    ini_write_real("game_state", "pixel_scale", pixel_scale);
    ini_write_real("game_state", "fullscreen", window_get_fullscreen());
}

var item_count = 0;
with(item)
{
    if (save)
    {
        save_item(self, item_count++);
    }
}
ini_write_real("game_state", "item_count", item_count);
ini_write_real("game_state", "last_date", date_current_datetime());
ini_write_real("game_state", "time_span", aanspoeler.time_span);

// save man
ini_write_real("man_section", "x_pos", man.x);
ini_write_real("man_section", "y_pos", man.y);
ini_close();


show_debug_message("Saved the game.");

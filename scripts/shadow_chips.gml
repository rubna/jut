///shadow_chips()
var gz = max(world.water_height, ground_z + in_put * 6);
var xoff = lengthdir_x((z - gz) * camera.shadow_scale, camera.shadow_dir);
var yoff = lengthdir_y((z - gz) * camera.shadow_scale, camera.shadow_dir) * camera.yscale;

// teken schaduw
var sx = lengthdir_x(width, image_angle + 90);
var sy = lengthdir_y(width, image_angle + 90);
var sx1 = lengthdir_x(width, image_angle + angle_off + 90);
var sy1 = lengthdir_y(width, image_angle + angle_off + 90);
var sx2 = lengthdir_x(width, image_angle - angle_off - 90);
var sy2 = lengthdir_y(width, image_angle - angle_off - 90);

// bovenkant
draw_primitive_begin(pr_trianglestrip)
vertex(x1 + sx1 + xoff, y1 + sy1 + yoff + z - gz);
vertex(x1 - sx1 + xoff, y1 - sy1 + yoff + z - gz);
vertex(x + sx + xoff, y + sy - gz + yoff);
vertex(x - sx + xoff, y - sy - gz + yoff);
vertex(x2 - sx2 + xoff, y2 - sy2 + yoff + z - gz);
vertex(x2 + sx2 + xoff, y2 + sy2 + yoff + z - gz);
draw_primitive_end();

///shadow_flesje();
if (leader && ander != noone)
{
    width *= 0.9;
    // calculate shadow direction
    var xsh = x + lengthdir_x((z - ground_z) * camera.shadow_scale, camera.shadow_dir);
    var ysh = y + lengthdir_y((z - ground_z) * camera.shadow_scale, camera.shadow_dir) * camera.yscale;
    
    var axsh = ander.x + lengthdir_x((ander.z - ander.ground_z) * camera.shadow_scale, camera.shadow_dir);
    var aysh = ander.y + lengthdir_y((ander.z - ander.ground_z) * camera.shadow_scale, camera.shadow_dir) * camera.yscale;
    var dr = point_direction(xsh, ysh, axsh, aysh);
    
    // dopje
    var t = 1.3;
    circle_3d_shadow(lerp(ander.x, x, t), lerp(ander.y, y, t), lerp(ander.z, z, t), width / 3, 0.66, 1, dr);
    t = 1.4;
    circle_3d_shadow(lerp(ander.x, x, t), lerp(ander.y, y, t), lerp(ander.z, z, t), width / 3, 0.66, 1, dr);
    
    // base
    line_3d_shadow(x, y, z, ander.x, ander.y, ander.z, width);
    circle_3d_shadow(x, y, z, width / 2, 1, 1);
    
     // uiteindes
    var side = lengthdir_y(1, dr);
    circle_3d_shadow(ander.x, ander.y, ander.z, width / 2,  side * 0.75, 1, dr);
    width /= 0.9;
}

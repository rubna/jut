///shadow_man();
with(body)
{
    var a = lengthdir_y(5, other.voet_t[0] * 180) + lengthdir_y(-5, other.voet_t[1] * 180)
    a = lengthdir_y(a, other.voet_dir - 90);
    a -= lengthdir_y(5 * wobble, wobblerot);
    sprite_shadow(spr_net, 0, x, y, z, 0.4 * xs, -0.4 * xs, -a);
}

// voeten
for (var i = 0; i < 2; i++)
{
    var side = i * 2 - 1;
    var heup_x = body.x + side_x * heup_w * side;
    var heup_y = body.y + side_y * heup_w * side;
    var heup_z = body.z - body.radius * 0.33;
    quadratic_bezier_3d_water_shadow(heup_x, heup_y, heup_z, 
                        heup_x, heup_y, heup_z - radius * 1, 
                        voet_x[i], voet_y[i], voet_z[i] + radius * 2,
                        voet_x[i], voet_y[i], voet_z[i], 3, 5);
    
    // tenen
    if (voet_z[i] > world.water_height)
        for (var j = 0; j < 3; j ++)
        {
            var f = j / 3;
            if (i == 0)
                f = 1 - f;
            var a = voet_a[i] + lerp(-30, 30, f);
            
            f = j / 3;
            var teen_len = radius * lerp(1, 0.66, f) * 0.8;
            var teen_w   = 4 * lerp(1, 1.4, f); 
            
            var teen_x = voet_x[i] + lengthdir_x(teen_len, a);
            var teen_y = voet_y[i] + lengthdir_y(teen_len, a);
            var teen_z = voet_z[i] + lengthdir_y(teen_len, voet_t[i] * 360) * 0.75;
            line_3d_shadow(voet_x[i], voet_y[i], voet_z[i], 
                    teen_x, teen_y, teen_z, teen_w);
            teen_w /= 2;
            circle_3d_shadow(teen_x, teen_y, teen_z, teen_w);
       } 
}

// handen
for (var i = 0; i < 2; i++)
{
    var side = i * 2 - 1;
    var heup_x = body.x + hand_side_x * schouder_w * side;
    var heup_y = body.y + hand_side_y * schouder_w * side;
    var heup_z = body.z + body.radius * 0.5;
    
    bezier_3d_water_shadow(heup_x, heup_y, heup_z,
                heup_x + hand_side_x * radius * 2 * side, heup_y + hand_side_y * radius * 2 * side, heup_z,
                hand_obj[i].x, hand_obj[i].y, hand_obj[i].z, 3, 3);
    if (hand_obj[i].z >= world.water_height)
        circle_3d_shadow(hand_obj[i].x, hand_obj[i].y, hand_obj[i].z, 5);
}

// nek
bezier_3d_shadow(body.x, body.y, body.z + radius, 
          body.x - forward_x * radius * 0, body.y - forward_y * radius * 0, (body.z + hoofd.z) / 2, 
          hoofd.x, hoofd.y, hoofd.z, 3, 2);
          
// teken net voorkant
with(body)
{
    var a = lengthdir_y(8, other.voet_t[0] * 180) + lengthdir_y(-8, other.voet_t[1] * 180)
    a = lengthdir_y(a, other.voet_dir - 90);
    a += lengthdir_y(8 * wobble, wobblerot);
    xs *= 0.9 + lengthdir_x(0.05, adem_rot);
    ys *= 0.9 + lengthdir_y(0.05, adem_rot - 60);
    sprite_shadow(spr_net, 1, x + other.forward_x * radius * 0.33, y + other.forward_y * radius * 0.33 + 0.2 * radius, z, 0.4 * xs, -0.4 * ys, -a);
}

// hoofd
//draw_set_color(color_hex($F0FFCC));
with(hoofd)
{
    circle_3d_shadow(x, y, z, lerp(radius, 0, other.crouch));
    
    // hoedje
    //sprite_shadow(spr_hoed, 0, x, y, z, 0.26, -0.26, -(other.body.x - x) * 3);
}

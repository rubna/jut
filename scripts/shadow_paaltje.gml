line_3d_shadow_known(x, y, z, x + xoff, y, z + zoff, z, z, width);

if (next)
{
    line_3d_shadow_known(lerp(x, x + xoff, hek_t), y, lerp(z, z + zoff, hek_t), 
        lerp(next.x, next.x + next.xoff, next.hek_t), next.y, lerp(next.z, next.z + next.zoff, next.hek_t), z, next.z, width * 0.66);
}

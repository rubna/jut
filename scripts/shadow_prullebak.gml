///shadow_prullebak();
circle_3d_shadow_known(x, y, z, z, radius, 1, 0.75, 0);

var dr = point_direction(0, 0, xoff, -zoff) + 90;
circle_3d_shadow_known(x + xoff, y, z + zoff, z, radius, 1, 0.75);

var sx = lengthdir_x(1, camera.shadow_dir + 90);
var sy = lengthdir_y(0.75, camera.shadow_dir + 90);
draw_primitive_begin(pr_trianglestrip)
vertex_shadow_known(x - sx * radius, y - sy * radius, z, z);
vertex_shadow_known(x + sx * radius, y + sy * radius, z, z);
vertex_shadow_known(x + xoff - sx * radius, y - sy * radius, z + zoff, z)
vertex_shadow_known(x + xoff + sx * radius, y + sy * radius, z + zoff, z);
draw_primitive_end();


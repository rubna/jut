///shadow_seastar()
var gz = max(ground_z, world.water_height) - radius / 4;
circle_3d_shadow_known(x, y, z, gz, 4);
for (var i = 0; i < hoeken; i++)
{
    //line_3d(xx[i], yy[i], zz[i], xx[next], yy[next], zz[next], 5 * scale);
    line_3d_shadow_known(x, y, z, xx[i], yy[i], zz[i], gz, gz, 5 * scale);
    circle_3d_shadow_known(xx[i], yy[i], zz[i], gz, 2.5 * scale);
}

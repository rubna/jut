///shadow_tak
if (leader && ander != noone)
{
    var gz = max(ground_z, world.water_height);
    var agz = max(ander.ground_z, world.water_height);
    var x1 = x + lengthdir_x((z - gz) * camera.shadow_scale, camera.shadow_dir);
    var y1 = y + lengthdir_y((z - gz) * camera.shadow_scale, camera.shadow_dir) * camera.yscale;
    y1 -= gz;
    var x2 = ander.x + lengthdir_x((ander.z - agz) * camera.shadow_scale, camera.shadow_dir);
    var y2 = ander.y + lengthdir_y((ander.z - agz) * camera.shadow_scale, camera.shadow_dir) * camera.yscale;
    y2 -= agz;
    
    var xx = x1 + x2; xx/=2;
    var yy = y1 + y2; yy/=2;
    
    var dst = point_distance(x1, y1, x2, y2);
    var dr = point_direction(x1, y1, x2, y2);
    d3d_set_fog(true, c_black, 0, 0);
    // shadow offset
    
    circle_3d(x1, y1, 0, 5 * scale);
    draw_sprite_ext(sprite_index, image_index, (xx - camera.x) * camera.zoom, (yy - camera.y) * camera.zoom, dst / sprite_width * camera.zoom, scale * camera.zoom, dr + camera.angle, -1, 1);
    d3d_set_fog(false, c_black, 0, 0);
}

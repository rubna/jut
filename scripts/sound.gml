///sound(sound, x{=xview}, y{=yview}, volume{= 1}, pitch{=1}, loop{=false});
var snd, xx, yy, vol, pitch, loop;
snd = argument[0];
xx = camera.center_x;
if (argument_count > 1)
    xx = argument[1];
yy = camera.center_y;
if (argument_count > 2)
    yy = argument[2];
vol = 1;
if (argument_count > 3)
    vol = argument[3];
pitch = 1;
if (argument_count > 4)
    pitch = argument[4];
loop = false;
if (argument_count > 5)
    loop = argument[5]

var s = audio_play_sound(snd, 1, loop);
var dst = point_distance(xx, yy, 
                         camera.center_x, camera.center_y);
vol = max(0, vol * (1 - dst / (view_wview / camera.zoom)));
audio_sound_gain(s, vol, 1);
audio_sound_pitch(s, pitch);
return s;

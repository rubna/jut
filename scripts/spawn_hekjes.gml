///spawn_hekjes();

var xx = world.x + 900;
var yy = world.y - 70;
var dr = -100;
var dist = 200;
var last = noone;
for (var i = 0; i < 20; i++)
{
    var new = instance_create(xx, yy, paaltje);
    new.z = get_height(xx, yy);
    if (i == 8)
    {
        var n= add_put_muur(0.25, 0.33);
        new_static(last.x, last.y, n.x, n.y, 10, true, 1);
        m = add_put_muur(0.21, 0.26);
        //n.next = m; 
        new_static(n.x, n.y, m.x, m.y, 10, true, 1); n = m; 
        m = add_put_muur(0.13, 0.25);
        //n.next = m; 
        new_static(n.x, n.y, m.x, m.y, 10, true, 1); n = m; 
        m = add_put_muur(0.12, 0.34);
        //n.next = m; 
        new_static(n.x, n.y, m.x, m.y, 10, true, 1); n = m; 
        m = add_put_muur(0.16, 0.39);
        //n.next = m; 
        new_static(n.x, n.y, m.x, m.y, 10, true, 1); n = m; 
        m = add_put_muur(0.22, 0.4);
        //n.next = m; 
        new_static(n.x, n.y, m.x, m.y, 10, true, 1); n = m;
        last = m;
        //last = noone;
    }
    else
    {
        if (last != noone)
        {
            if (i!=9) // paaltje na paadje
                last.next = new;
            new_static(last.x, last.y, new.x, new.y, 15, false, 1);
        }
        last = new;
        
    }
    var dist = random_range(150, 200) * 0.66;
    var dr = random_range(-120, -105);
    xx += lengthdir_x(dist, dr);
    yy += lengthdir_y(dist, dr);
}

///spawn_item(x, y, z);

var obj = common_item();
if (random(1) < 0.15)
    obj = special_item();
if (random(1) < 0.03)
    obj = rare_item();
var i = instance_create(argument0, argument1, obj);
i.z = argument2;
aanspoeler.aantal_items++;
return i;

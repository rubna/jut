///spawn_palen();

var xx = world.x + 1350;
var yy = world.y - 50;
var dr = -100;
var dist = 600;
for (var i = 0; i < 7; i++)
{
    var new = instance_create(xx, yy, paal);
    new.z = get_height(xx, yy);
    
    var dist = random_range(550, 650) * 0.66;
    var dr = random_range(-120, -105);
    xx += lengthdir_x(dist, dr);
    yy += lengthdir_y(dist, dr);
}

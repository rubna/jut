///spawn_prullebakken();

var xx = world.x + 900;
var yy = world.y + 500;
var dr = -100;
var dist = 600;
for (var i = 0; i < 3; i++)
{
    var new = instance_create(xx, yy, prullebak);
    new.z = get_height(xx, yy);
    
    var dist = random_range(550, 650);
    var dr = random_range(-120, -105);
    xx += lengthdir_x(dist, dr);
    yy += lengthdir_y(dist, dr);
}

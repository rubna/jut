///spring_tong(sliert_i, i1, i2, dist, amount, pushOut {=true}, springIn{=true});
var ii, a, b, dst, amt, out, in;
ii = argument[0];
a = argument[1];
b = argument[2];
dst = argument[3];
amount = argument[4];
out = true;
if (argument_count > 5)
    out = argument[5];
in = true;
if (argument_count > 6)
    in = argument[6];
    
var xoff, yoff, zoff;
xoff = xx[ii, a] - xx[ii, b];
yoff = yy[ii, a] - yy[ii, b];
zoff = zz[ii, a] - zz[ii, b];

var dist;
dist = point_distance_3d(0, 0, 0, xoff, yoff, zoff);
dist -= dst;
if ((dist < 0 && out) || (dist > 0 && in))
{
    var angle_xy, angle_z;
    angle_xy = point_direction(0, 0, xoff, yoff);
    angle_z = point_direction(0, 0, point_distance(0, 0, xoff, yoff), zoff);
    xoff = lengthdir_x(lengthdir_x(dist, angle_z), angle_xy) / 2; 
    yoff = lengthdir_y(lengthdir_x(dist, angle_z), angle_xy) / 2; 
    zoff = lengthdir_y(dist, angle_z) / 2; 
    
    xsp[ii, a] -= xoff * amount;
    ysp[ii, a] -= yoff * amount;
    zsp[ii, a] -= zoff * amount;
    xsp[ii, b] += xoff * amount;
    ysp[ii, b] += yoff * amount;
    zsp[ii, b] += zoff * amount;
}

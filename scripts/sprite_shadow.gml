///sprite_shadow(sprite_index, image_index, x, y, z, xscale{=1}, yscale{=1}, angle{=0}, color{=-1}, alpha{=1});
var spr = argument[0];
var img = argument[1];
var xx = argument[2];
var yy = argument[3];
var zz = argument[4];
var xsc = 1;
if (argument_count > 5)
    xsc = argument[5];
var ysc = 1;
if (argument_count > 6)
    ysc = argument[6];
var ang = 0;
if (argument_count > 7)
    ang = argument[7];
var blend = -1;
if (argument_count > 8)
    blend = argument[8];
var alpha = 1;
if (argument_count > 9)
    alpha = argument[9];

d3d_set_fog(true, c_black, 0, 0);
var gz = max(world.water_height, get_height(xx, yy, false));
// shadow offset
xx += lengthdir_x((zz - gz) * camera.shadow_scale, camera.shadow_dir);
yy += lengthdir_y((zz - gz) * camera.shadow_scale, camera.shadow_dir) * camera.yscale;

draw_sprite_ext(spr, img, (xx - camera.x) * camera.zoom, (yy - gz - camera.y) * camera.zoom, xsc * camera.zoom, ysc * camera.zoom, ang + camera.angle, blend, alpha);
d3d_set_fog(false, c_black, 0, 0);

///start_sound()
sound_started = true;

 
// sounds!
waad_sound = sound(sou_waden, camera.center_x, camera.center_y, 0, 1, true);
waad_volume = 0;
rustle_sound = sound(sou_plastic_rustle, camera.center_x, camera.center_y, 0, 1, true);
cans_sound = sound(sou_cans, camera.center_x, camera.center_y, 0, 1, true);
rustle_volume = 0;
duin_regen_ambience = sound(sou_duin_regen, camera.center_x, camera.center_y, 0, 1, true);
sea_ambience = sound(sou_sea2, camera.center_x, camera.center_y, 0, 0.9, true);
beach_ambience = sound(sou_beach_ambience, camera.center_x, camera.center_y, 0.6, 1, true);
wind_ambience = sound(sou_wind_ambience, camera.center_x, camera.center_y, 0, 0.8, true);
sea_regen_ambience = sound(sou_sea_rain_ambience, camera.center_x, camera.center_y, 0, 1, true);

//-- end

///surface(surface_id, x, y, xscale{=1}, yscale{=1}, angle{=0}, color{=-1}, alpha{=1});
var spr = argument[0];
var xx = argument[1];
var yy = argument[2];
var xsc = 1;
if (argument_count > 3)
    xsc = argument[3];
var ysc = 1;
if (argument_count > 4)
    ysc = argument[4];
var ang = 0;
if (argument_count > 5)
    ang = argument[5];
var blend = -1;
if (argument_count > 6)
    blend = argument[6];
var alpha = 1;
if (argument_count > 7)
    alpha = argument[7];

draw_surface_ext(spr, (xx - camera.x) * camera.zoom, (yy - camera.y) * camera.zoom, xsc * camera.zoom, ysc * camera.zoom, ang + camera.angle, blend, alpha);

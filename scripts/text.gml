///text(x, y, string, xscale{=0}, yscale{=0}, angle{=0})
var xsc = 1;
if (argument_count > 3)
    xsc = argument[3];
var ysc = 1;
if (argument_count > 4)
    ysc = argument[4];
var ang = 0;
if (argument_count > 5)
    ang = argument[5];
draw_text_transformed((argument[0] - camera.x) * camera.zoom, (argument[1] - camera.y) * camera.zoom, argument[2], xsc * camera.zoom * 0.5, ysc * camera.zoom * 0.5, ang);

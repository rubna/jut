///vertex_shadow_known(x, y, z, ground_z);
var xp = argument0;
var yp = argument1;
var zp = argument2;
var gz = argument3;
// shadow offset
gz = max(world.water_height, gz);
xp += lengthdir_x((zp - gz) * camera.shadow_scale, camera.shadow_dir);
yp += lengthdir_y((zp - gz) * camera.shadow_scale, camera.shadow_dir) * camera.yscale;
yp -= gz;
draw_vertex((xp - camera.x) * camera.zoom, (yp - camera.y) * camera.zoom);

stap_i = 1 - stap_i;
var side = stap_i * 2 - 1;

// set last voeten
voet_xlast[stap_i] = voet_xtarg[stap_i];
voet_ylast[stap_i] = voet_ytarg[stap_i];
voet_zlast[stap_i] = voet_ztarg[stap_i];
voet_alast[stap_i] = voet_atarg[stap_i];

// set nieuwe targets
var f = random_range(75, 85) * 1.25;
voet_xtarg[stap_i] = x + loop_side_x * voet_w * side + hspd * f;
voet_ytarg[stap_i] = y + loop_side_y * voet_w * side + vspd * f;
voet_ztarg[stap_i] = get_height(voet_xtarg[stap_i], voet_ytarg[stap_i]);
voet_atarg[stap_i] = loop_dir;
voet_t[stap_i] = 0;

// check collisions...
voetje[stap_i].x = voet_xtarg[stap_i];
voetje[stap_i].y = voet_ytarg[stap_i];
voetje[stap_i].z = voet_ztarg[stap_i];
with(voetje[stap_i])
{
    collide_statics(0, 0, 0);
    if (y < 300)
        y = 300;
    //if (x < 200)
    //    x = 200;
    if (x > world.sprite_width - 300)
        x = world.sprite_width - 300;
    if (y > world.sprite_height - 80)
        y = world.sprite_height - 80;
}

voet_xtarg[stap_i] = voetje[stap_i].x;
voet_ytarg[stap_i] = voetje[stap_i].y;
voet_ztarg[stap_i] = voetje[stap_i].z;

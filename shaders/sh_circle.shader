//
// Simple passthrough vertex shader
//
attribute vec3 in_Position;                  // (x,y,z)
//attribute vec3 in_Normal;                  // (x,y,z)     unused in this shader.	
attribute vec4 in_Colour;                    // (r,g,b,a)
attribute vec2 in_TextureCoord;              // (u,v)

varying vec2 v_vTexcoord;
varying vec4 v_vColour;

void main()
{
    vec4 object_space_pos = vec4( in_Position.x, in_Position.y, in_Position.z, 1.0);
    gl_Position = gm_Matrices[MATRIX_WORLD_VIEW_PROJECTION] * object_space_pos;
    
    v_vColour = in_Colour;
    v_vTexcoord = in_TextureCoord;
}

//######################_==_YOYO_SHADER_MARKER_==_######################@~






//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;
uniform float radius;
uniform float aspect;

void main()
{
    vec4 col = v_vColour;
    vec2 pos = v_vTexcoord;
    vec2 mid = vec2(0.5, 0.5);
    vec2 off = pos - mid;
    float dst = sqrt((off.x * aspect) * (off.x * aspect) + off.y * off.y);
    float f = sign(radius - dst) / 2. + 0.5;
    col.a *= f;
    //col.g *= f;
    //col.b *= f;
    gl_FragColor = col * texture2D( gm_BaseTexture, v_vTexcoord );
}


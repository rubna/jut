//
// Simple passthrough vertex shader
//
attribute vec3 in_Position;                  // (x,y,z)
//attribute vec3 in_Normal;                  // (x,y,z)     unused in this shader.	
attribute vec4 in_Colour;                    // (r,g,b,a)
attribute vec2 in_TextureCoord;              // (u,v)

varying vec2 v_vTexcoord;
varying vec4 v_vColour;

void main()
{
    vec4 object_space_pos = vec4( in_Position.x, in_Position.y, in_Position.z, 1.0);
    gl_Position = gm_Matrices[MATRIX_WORLD_VIEW_PROJECTION] * object_space_pos;
    
    v_vColour = in_Colour;
    v_vTexcoord = in_TextureCoord;
}

//######################_==_YOYO_SHADER_MARKER_==_######################@~
//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;
const vec4 col_water = vec4(78. / 255., 104. / 255., 94./ 255., 1.);
const vec4 col_licht_water = vec4(84. / 255., 112. / 255., 102./ 255., 1.);
const vec4 col_nat_zand = vec4(223. / 255., 214. / 255., 188./ 255., 1.);//229

const vec4 col_zand = vec4(219. / 255., 196. / 255., 157./ 255., 1.);
const vec4 col_hoogzand = vec4(226. / 255., 206. / 255., 170./ 255., 1.);
const vec4 col_surf = vec4(1., 1., 1., 1.);
const vec4 col_surf_dk = vec4(0.98, 0.98, 0.98, 1.);
const vec4 col_gras = vec4(183. / 255., 180. / 255., 135./ 255., 1.);
const vec4 col_gras_dk = vec4(152. / 255., 160. / 255., 119./ 255., 1.);
const vec4 col_gras_dk_dk = vec4(135. / 255., 135. / 255., 98./ 255., 1.);
    
uniform float water_height;
uniform float nat_height;
uniform float surf_height;
uniform float wind_dir;
uniform float wind_amount;

vec4 merge_color(vec4 col_1, vec4 col_2, float t_1)
{
    float t_2 = 1. - t_1;
    return vec4(col_1.r * t_1 + col_2.r * t_2, 
                col_1.g * t_1 + col_2.g * t_2, 
                col_1.b * t_1 + col_2.b * t_2, 1.);
}

void main()
{
    vec4 base_color =  texture2D( gm_BaseTexture, v_vTexcoord );
    float height = base_color.r;
    
    // wobble
    height += (sin(v_vTexcoord.x * 130.) + cos(v_vTexcoord.y * 170.)) * 0.02;
    
    // gras shader
    vec2 graspos = v_vTexcoord;
    //graspos.x -= cos(wind_dir) * wind_amount * 0.02;
    //graspos.y -= sin(wind_dir) * wind_amount * 0.02;
    float gras_wobble = (-abs(cos((graspos.y * v_vTexcoord.y + graspos.x) * 5000.)) * 10. + cos(v_vTexcoord.y *  5000.)) * 0.005;
    gras_wobble += (sin(graspos.x * 500.) + cos(graspos.y * 600.)) * 0.02;
    float gras_t = floor(0.85 - height - gras_wobble + 1.);
    
    // laag 2
    graspos = v_vTexcoord;
    graspos.x -= cos(wind_dir) * wind_amount * 0.15;
    graspos.y -= sin(wind_dir) * wind_amount * 0.15;
    
    gras_wobble = (-abs(cos((graspos.y * v_vTexcoord.y + graspos.x) * 5000.)) * 10. + cos(v_vTexcoord.y *  5000.)) * 0.005;
    gras_wobble += (sin(graspos.x * 500.) + cos(graspos.y * 600.)) * 0.02;
    float gras_height = texture2D( gm_BaseTexture, graspos ).r;
    float gras_t_dk = floor(0.94 - gras_height + gras_wobble + 1.);
    
    // laag 3
    graspos = v_vTexcoord;
    graspos.x -= cos(wind_dir) * wind_amount * 0.33;
    graspos.y -= sin(wind_dir) * wind_amount * 0.33;
    
    gras_wobble = (-abs(cos((graspos.y * v_vTexcoord.y + graspos.x) * 5000.)) * 10. + cos(v_vTexcoord.y *  5000.)) * 0.005;
    gras_wobble += (sin(v_vTexcoord.x * 500.) + cos(v_vTexcoord.y * 600.)) * 0.02;
    
    gras_height = texture2D( gm_BaseTexture, graspos ).r;
    float gras_t_dk_dk = floor(0.95 - gras_height - gras_wobble / 2. + 1.);
    
    float surf_wobble = sin(graspos.y * 4000. + graspos.x * 3000.)  * 0.002;
    surf_wobble += cos(graspos.x * v_vTexcoord.x * 4500.) * 0.001;
    
    // rest van variabelen
    float water_t = floor(height - water_height - surf_wobble + 1.);
    float donker_t = floor(height - (surf_height - 0.1) - surf_wobble * 0.5 + 1.);
    float surf_t = floor(height - surf_height + surf_wobble + 1.);
    //float surf_t_dk = floor(height - (water_height - 0.01) + 1.);
    float nat_t = floor(height - nat_height + 1.);
    float hoog_t = floor(0.7 - height + 1.);
    
    vec4 zand_col = merge_color(col_zand, col_nat_zand, nat_t);
    zand_col = merge_color(zand_col, col_surf, surf_t);
    vec4 land_col = merge_color(zand_col, col_hoogzand, hoog_t);
    land_col = merge_color(land_col, col_gras, gras_t);
    land_col = merge_color(land_col, col_gras_dk, gras_t_dk);
    land_col = merge_color(land_col, col_gras_dk_dk, gras_t_dk_dk);
    
    vec4 water_col = merge_color(col_licht_water, col_water, donker_t);
    //water_col = merge_color(col_surf, water_col, surf_t);
    //water_col = merge_color(col_surf_dk, water_col, surf_t_dk);
    vec4 col = merge_color(land_col, water_col, water_t);
    gl_FragColor = v_vColour * col;
}

